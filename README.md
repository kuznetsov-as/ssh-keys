# Настройка доступа на сервер через ssh ключи

## Доступ из winsdows в linux

С помощью программы PuTTYgen генерируем ключи (RSA/2048), затем сохраняем их в любом удобном для себя месте.
Заходим на сервер под юзером к которому будем подключаться, проверяем наличие файла **authorized_keys** по пути **~/.ssh/authorized_keys**

<details>
  <summary>Если нет</summary>
  
Если нет, то создаем папку **.ssh**
  ``` bash
  mkdir ~/.ssh
  ```
Выдаем на нее права
``` bash
chmod 700 ~/.ssh
```
Создаем **authorized_keys** и кладем в него свой публичный ключ
``` bash
vim ~/.ssh/authorized_keys
```
Вводим публичный ключ в **~/.ssh/authorized_keys** в одну строку ( **ssh-rsa {your_public_key}** при этом ставим не более одного пробела между **ssh-rsa** и ключом)

Выполняем:
``` bash
chmod 600 ~/.ssh/authorized_keys && chown $USER:$USER ~/.ssh -R
```

Из под рута (или через sudo) убеждаемся в том, что **/etc/ssh/sshd_config**  содержит строку **AuthorizedKeysFile %h/.ssh/authorized_keys**

Если ее там нет - добавляем.

Перезагружаем:
``` bash
sudo service ssh restart
```
</details>

<details>
  <summary>Если есть</summary>

Вводим публичный ключ в **~/.ssh/authorized_keys** в одну строку ( **ssh-rsa {your_public_key}** при этом ставим не более одного пробела между **ssh-rsa** и ключом)

Выполняем:
``` bash
chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys && chown $USER:$USER ~/.ssh -R
```

Из под рута (или через sudo) убеждаемся в том, что **/etc/ssh/sshd_config**  содержит строку **AuthorizedKeysFile %h/.ssh/authorized_keys**

Если ее там нет - добавляем.

Перезагружаем:
``` bash
sudo service ssh restart
```
</details>

После вышеописанных действий указываем путь до приватного ключа в PuTTY и можем подключаться, должен перестать спрашивать пароль.

## Доступ из linux в linux
Создаем ключи:
``` bash
ssh-keygen -b 2048 -t rsa
```
Оно спросит путь до файла, в который будет записывать результат генерации, указываем, например **/home/user/ssh-keys/key**

В результате по пути **/home/user/ssh-keys/** появятся файлы **key** и **key.pub**

Заходим на машину, к которой будем подключаться, под юзером, к которому будем подключаться, проверяем наличие файла **authorized_keys** по пути **~/.ssh/authorized_keys**


<details>
  <summary>Если нет</summary>
  
Если нет, то создаем папку **.ssh**
  ``` bash
  mkdir ~/.ssh
  ```
Выдаем на нее права
``` bash
chmod 700 ~/.ssh
```
Создаем **authorized_keys** и кладем в него содержимое **key.pub**
``` bash
vim ~/.ssh/authorized_keys
```
Вводим публичный ключ в **~/.ssh/authorized_keys** в одну строку ( **ssh-rsa {your_public_key}** при этом ставим не более одного пробела между **ssh-rsa** и ключом)

Выполняем:
``` bash
chmod 600 ~/.ssh/authorized_keys && chown $USER:$USER ~/.ssh -R
```

Из под рута (или через sudo) убеждаемся в том, что **/etc/ssh/sshd_config**  содержит строку **AuthorizedKeysFile %h/.ssh/authorized_keys**

Если ее там нет - добавляем.

Перезагружаем:
``` bash
sudo service ssh restart
```
</details>

<details>
  <summary>Если есть</summary>

Вводим публичный ключ (содержимое **key.pub**) в **~/.ssh/authorized_keys** в одну строку ( **ssh-rsa {your_public_key}** при этом ставим не более одного пробела между **ssh-rsa** и ключом)

Выполняем:
``` bash
chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys && chown $USER:$USER ~/.ssh -R
```

Из под рута (или через sudo) убеждаемся в том, что **/etc/ssh/sshd_config**  содержит строку **AuthorizedKeysFile %h/.ssh/authorized_keys**

Если ее там нет - добавляем.

Перезагружаем:
``` bash
sudo service ssh restart
```
</details>

В зависимости от способа подключения указываем на клиентской машине (с которой будем подключаться) путь до приватного ключа (содержимое **key**)

## Подключение к linux при помощи gitlab runner запущенного в докере
Выполняем шаги обозначенные в предыдущем пункте (**Доступ из linux в linux**). Затем настраиваем секреты гитлаба:
**Settings -> CI/CD -> Variables**

- SERVER_SSH_USER
- SERVER_SSH_ADDRES
- SERVER_SSH_PRIVATE_KEY

Для всех трех переменных указываем такие настройки:
![изображение.JPG](img/variables.JPG)

В **SERVER_SSH_PRIVATE_KEY** записываем содержимое файла **key** 

Формат:
``` bash
-----BEGIN OPENSSH PRIVATE KEY-----
your key
-----END OPENSSH PRIVATE KEY-----

```
Настраиваем пайплайн **.gitlab-ci.yml**
``` bash
stages:
  - build

.ssh_helper: &ssh_helper |
  function ssh_init() {
      SSH_PRIVATE_KEY="$1"
      SSH_KNOWN_HOSTS="$2"
      test -n "$SSH_PRIVATE_KEY" || ( echo "missing variable SSH_PRIVATE_KEY" && exit 1)
      test -n "$SSH_KNOWN_HOSTS" || ( echo "missing variable SSH_KNOWN_HOSTS" && exit 1)
      which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
      eval $(ssh-agent -s)
      echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
      mkdir -p ~/.ssh
      chmod 700 ~/.ssh
      ssh-keyscan -H "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
      chmod 644 ~/.ssh/known_hosts
      cat ~/.ssh/known_hosts
      echo ssh_init finished
  }

build-job:
  image: golang:latest
  stage: build
  before_script:
    - *ssh_helper
  script:
    - ssh_init "$SERVER_SSH_PRIVATE_KEY" "$SERVER_SSH_ADDRES"
    - echo $SERVER_SSH_ADDRES $SERVER_SSH_USER
    - mkdir test
    - echo 1 > test/1.txt
    - echo 2 > test/2.txt
    - ls
    - ls test
    - tar -czvf test.tar.gz test
    - ls
    - scp -r test.tar.gz $SERVER_SSH_USER@$SERVER_SSH_ADDRES:/home/$SERVER_SSH_USER/
```
Запускаем его, в итоге имеем такой результат выполнения:
![изображение.JPG](img/result.jpg)

В корневой папке юзера, в которого подключались через ssh появляется файл **test.tar.gz**:
![изображение.JPG](img/testtar.JPG)

## Полезные ссылки
[Trying to do ssh authentication with key files: server refused our key](https://askubuntu.com/questions/306798/trying-to-do-ssh-authentication-with-key-files-server-refused-our-key)

